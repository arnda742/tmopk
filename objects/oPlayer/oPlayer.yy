{
    "id": "c6dfd5a4-428b-4292-aefd-8ca064c86a42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "e6375d47-31d7-484e-9b83-ee8ec42fa903",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6dfd5a4-428b-4292-aefd-8ca064c86a42"
        },
        {
            "id": "6926755f-0118-4390-bf5f-7761cd8c12b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6dfd5a4-428b-4292-aefd-8ca064c86a42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "781188d2-e3ec-497f-af69-ec8698c7db29",
    "visible": true
}