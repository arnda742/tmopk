{
    "id": "a718534b-fc32-4e9f-b0fc-d972f146397a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Swall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6ec6535-51ef-4c7e-a716-fbeae943af25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a718534b-fc32-4e9f-b0fc-d972f146397a",
            "compositeImage": {
                "id": "80a5fe1e-76e3-4a33-a45d-1e3c01ed3751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6ec6535-51ef-4c7e-a716-fbeae943af25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cffe92d-0913-4295-9fa6-25312230ae22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6ec6535-51ef-4c7e-a716-fbeae943af25",
                    "LayerId": "aa2da722-9904-4076-a162-1a34336f3713"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aa2da722-9904-4076-a162-1a34336f3713",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a718534b-fc32-4e9f-b0fc-d972f146397a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}