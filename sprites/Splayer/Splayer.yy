{
    "id": "781188d2-e3ec-497f-af69-ec8698c7db29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Splayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b00db413-5370-494a-be9f-b36866629f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "781188d2-e3ec-497f-af69-ec8698c7db29",
            "compositeImage": {
                "id": "a5165e0f-e078-443d-aedb-ae08244b8b96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00db413-5370-494a-be9f-b36866629f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12280247-282a-4a3e-ab60-5df5d4cbd9b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00db413-5370-494a-be9f-b36866629f3e",
                    "LayerId": "e1129282-06f3-45a4-b236-9055ab1f483c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1129282-06f3-45a4-b236-9055ab1f483c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "781188d2-e3ec-497f-af69-ec8698c7db29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}